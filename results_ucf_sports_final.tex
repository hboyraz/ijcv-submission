The UCF Sports dataset contains $150$ video sequences and includes $10$ human actions.
% diving, golf swinging, kicking (a ball), weight lifting, horse riding, running, skateboarding, swinging (on the pommel horse and on the floor), swinging (at the high bar) and walking.
It is a challenging dataset due to large variations in camera motion, object appearance and pose, object scale, viewpoint, cluttered background and illumination conditions.

\subsubsection{Accuracy Results}
\label{sec:UCFSportsActionRec}
%While it is common to use a Leave-One-Out-Cross-Validation (LOOCV) testing methodology when conducting experiments with the UCF Sports dataset, Lan et al.~\cite{Lan11} have recently pointed out that many of the videos in this dataset are clips taken from a longer video.  This is problematic when conducting LOOCV tests because several training clips will often be drawn from the same video as the testing clip.  This causes the classifier to perform best when it effectively memorizes the appearance of the training clips to exploit the strong inherent context correlation among clips drawn from the same video segment.
While it is common to use a Leave-One-Out-Cross-Validation (LOOCV) testing methodology when conducting experiments with the UCF Sports dataset, Lan et al.~\cite{Lan11} have recently pointed out that many of the videos in this dataset are clips taken from a longer video.  This is problematic when conducting LOOCV tests because several training clips will often be drawn from the same video as the testing clip. 

In order to overcome this issue,~\cite{Lan11} suggest using approximately a third of the videos from each action class for testing while the remaining videos are reserved for training. 

%We use the same train/test split\footnote{Available at \url{http://www.sfu.ca/~tla58/other/train_test_split}}.

\begin{table}[htb]
\begin{center}
\setlength{\tabcolsep}{4pt}
\begin{tabular}{|c|c|p{10cm}}
\hline
\textbf{Method}       & \textbf{Accuracy(\%)} \\ \hline \hline
%Global BoW+SVM [Linear]  & $65.95$ \\
%Our Method [Linear] &72.86\\
%\hline
Lan et al.\cite{Lan11} & $73.1$\textcolor{red}{*}\\ 
Shapovalova et al.\cite{Shapovalova12} & $75.3$\\
Raptis et al.\cite{Raptis12} & $79.4$\textcolor{red}{*}\\ \hline
Global BOW  & $70.21$ \\
Our Method & $\mathbf{80.95}$\\
\hline
\end{tabular}
\end{center}
\caption{Mean per-class action recognition accuracies (split) on the UCF Sports dataset using STIP features. \textcolor{red}{*} Both ~\cite{Lan11} and ~\cite{Raptis12} use ground truth annotations during training where as our model is weakly supervised and does not require ground truth annotations.}
\label{table:split}
\end{table}

%We observe that using a global bag-of-words approach only leads to an approximate $66\%$ accuracy. This is similar to results reported in~\cite{Lan11,Raptis12,Shapovalova12} and demonstrates the inability of the system to differentiate between discriminative vs non-discriminative features.
%While a slight improvement is achieved by switching from a linear to non-linear kernel, our localization based-system is able to improve the classification accuracy of the global bag-of-words system by more than 10\%. 

Table~\ref{table:split} shows results using the train-test split suggested in~\cite{Lan11} using STIP features. As shown in the table, our localization based-system is able to improve the classification accuracy of the baseline global bag-of-words system by more than 10\%. Compared with recent action recognition systems, Table \ref{table:split} also shows two important aspects of the performance of this system: 
%1) The $80.95\%$ recognition accuracy of our system is more than $5\%$ better than the accuracy of the recently proposed weakly-supervised system in \cite{Shapovalova12}, with the added advantage of not requiring any object saliency detector for limiting candidate sub-regions. This is, in part, due to the ability of our system to learn localizers that are trained to find discriminative regions. 2) Our system is competitive with recently proposed methods that require hand-annotated regions in the training data \cite{Lan11,Raptis12}, significantly improving on \cite{Lan11}.
\begin{enumerate}
\item The $80.95\%$ recognition accuracy of our system is more than $5\%$ better than the accuracy of the recently proposed weakly-supervised system in \cite{Shapovalova12}, with the added advantage of not requiring any object saliency detector for limiting candidate sub-regions. This is, in part, due to the ability of our system to learn localizers that are trained to find discriminative regions.
\item Our system is competitive with recently proposed methods that require hand-annotated regions in the training data \cite{Lan11,Raptis12}, significantly improving on \cite{Lan11}.
\end{enumerate}

We also conducted experiments using different numbers of discriminative sub-region localizers. The corresponding classification accuracies are provided in Table~\ref{table:numLocalizers}. When the number of localizers is zero, the whole frame is considered, and the results are similar to the global bag of words model. Even with only $2$ localizers, the classification performance significantly increases, peaking with $10$ localizers. Increasing the number of localizers further, results in a slight drop in performance which may be due to the model overfitting on the training data.

\begin{table}[htb]
\begin{center}
\setlength{\tabcolsep}{4pt}
\begin{tabular}{|c|c|p{10cm}}
\hline
\textbf{\# of detectors (D)}  &\textbf{Accuracy(\%)} \\ \hline \hline
0  & $71.78$ \\
1  & $73.45$ \\
2 & $77.85$ \\ 
5 & $79.52$\\
10 & $\mathbf{80.95}$\\
15 & 78.45 \\
\hline
\end{tabular}
\end{center}
\caption{Classification accuracy w.r.t. the number of discriminative region detectors. We observe that increasing localizers improves recognition performance up until $D=10$. Beyond that, there is a drop in performance, which can be attributed to overfitting.}
\label{table:numLocalizers}
\end{table}

\begin{figure}[htb]
\centering
\includegraphics[width=0.85\linewidth]{_figure_ucfsports_confmat}
\caption{Confusion matrix for the UCF Sports dataset using train/test split.}
\label{fig:ucfsports_confmat}
\end{figure}


\subsubsection{Localization Results}
\label{sec:UCFSportsActionLoc}
Because our method is trained using weakly-supervised data, this method is limited to learning to isolate discriminative sub-regions in the video.  However, the results in this section will show that this approach is able to actually locate the action with accuracy that is comparable to previous work that was trained with hand-annotated bounding boxes.

For action locations, we pick the sub-region with the highest probability in each video frame using Equation \eqref{eq:localizer_prob}. In order to evaluate how well our discriminative sub-regions are localizing actions, we use the same evaluation criterion given in \cite{Lan11}
%: we first compute the intersection-over-union score per frame using ${Area(R_f \cap R_f^g)}/{Area(R_f\cup R_f^g)}$ where $R_f$ is the detected sub-region in frame $f$ and $R_f^g$ is the ground-truth action bounding box. Then, we compute the average intersection-over-union score for a video using all frames in the video. If this score is greater than a threshold $\sigma$, we consider the video to be correctly localized. 
and compute the ROC curves for each action class. A video is considered as correctly predicted if both the prediction label and the localization match the ground truth. 

Figure~\ref{fig:long} shows our average ROC curve for action classes and the ROC curve from~\cite{Lan11} for $\sigma=0.2$. We use $\sigma=0.2$ for comparison since~\cite{Lan11} provides ROC curve only for $\sigma=0.2$. We also compute the area under the ROC curve (AUC) for different $\sigma$ values. Although our system has no access to ground-truth bounding boxes during training, while the system in \cite{Lan11} does, our system performs comparably with \cite{Lan11} and in many cases outperforms it.  

Figure~\ref{fig:ucfsports_localization} shows localization results obtained using our proposed technique that provide empirical evidence that it localizes the actual action well, despite only being trained to locate discriminative sub-regions. This indicates that the sub-regions containing the actual action tend to be the most discriminative sub-regions.

\begin{figure}[htb]
\begin{center}
\subfigure[]{
  \includegraphics[width=0.95\linewidth]{_figure_UCF_ROC.pdf}
}
\subfigure[]{
  \includegraphics[width=0.95\linewidth]{_figure_UCF_AUC.pdf}
}
\end{center}
\caption{Comparison of action localization performance against Lan et al. \cite{Lan11}. (a) ROC curves for $\sigma = 0.2$. (b) Area Under ROC for different $\sigma$. $\sigma$ is the threshold that determines if a video is correctly localized. Compared with \cite{Lan11}, which requires the action be manually located in the training data, our system produces comparable or improved results.  
}


\label{fig:long}
\label{fig:onecol}
\end{figure}

\begin{figure*}[t]
\centering
\subfigure{
  \includegraphics[width=0.17\linewidth]{_figure_sample_detections_ucfsports_diving.png}
}
\subfigure{
  \includegraphics[width=0.17\linewidth]{_figure_sample_detections_ucfsports_golfing.png}
}
\subfigure{
  \includegraphics[width=0.17\linewidth]{_figure_sample_detections_ucfsports_kicking.png}
}
\subfigure{
   \includegraphics[width=0.17\linewidth]{_figure_sample_detections_ucfsports_lifting.png}
}
\subfigure{
   \includegraphics[width=0.17\linewidth]{_figure_sample_detections_ucfsports_horseriding.png}

}
\subfigure{
   \includegraphics[width=0.17\linewidth]{_figure_sample_detections_ucfsports_walking.png}
}
\subfigure{
   \includegraphics[width=0.17\linewidth]{_figure_sample_detections_ucfsports_running.png}
}
\subfigure{
   \includegraphics[width=0.17\linewidth]{_figure_sample_detections_ucfsports_skating.png}
}
\subfigure{
   \includegraphics[width=0.17\linewidth]{_figure_sample_detections_ucfsports_benchswing.png}
}
\subfigure{
   \includegraphics[width=0.17\linewidth]{_figure_sample_detections_ucfsports_barswing.png}
}
\caption{We show localization results obtained using our method on the UCF Sports action dataset. We can see that our model is able to correctly localize action specific sub-regions as the best possible representation of the action being conducted in the video.}
\label{fig:ucfsports_localization}
\end{figure*}
