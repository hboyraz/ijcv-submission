
After computing the histograms, we use them to estimate the probability of action labels for each video. The key problem is that the system must aggregate the information in $D$ different localizers to produce this final probability. This aggregation is implemented using a two-layer feed forward neural network where the last layer is a C-way softmax classifier for action classification. 

\begin{figure}[ht]
\centering
\includegraphics[width=0.50\textwidth]{_figure_network.pdf}
\caption{Two-layer neural network for final classification where $B$ is number of input nodes, $M$ is number of hidden units and $C$ is the number of action classes.}
\label{fig:NN}
\end{figure}

For simplicity, we will first consider the case when there is a single localizer and then, extend it to multiple localizers. Figure~\ref{fig:NN} shows the network diagram for a single localizer. The input, hidden and output variables are represented by nodes and the weight parameters are represented by the links between nodes. The $\hat x_{i}$ represents the $i^{th}$ bin of the feature vector $\hat x$ and the $\hat x_{0}$ and $z_{0}$ represent the bias terms which are set to $1$. The input for the $j^{th}$ hidden unit is computed as the linear combination of the input variables as follows: 

\begin{equation}
\label{eq:a_j}
a_j = \displaystyle \sum_{i=1}^B{w_{ji}^{(1)}x_i } + w_{j0}^{(1)}
\end{equation}

\noindent where $B$ is total number of inputs and the superscript (1) indicates the first layer of the network. Output of the hidden units are computed using a logistic activation function $h$. 
The outputs of the hidden units are linearly combined as input to the output units

\begin{equation}
\label{eq:b_k}
b_k = \displaystyle \sum_{j=1}^M{w_{kj}^{(2)}z_j } + w_{k0}^{(2)}
\end{equation}

\noindent where $M$ is the number of hidden units, $k=1, ..., C$ and $C$ is the total number of action classes. Finally, the softmax activation function is used to provide the class probability outputs as follows: 

\begin{equation}
\label{eq:output_activation}
y_k = f(b_k) =\displaystyle \frac{e^{b_k}}{ \sum_{k'\in C}e^{b_{k'}}}
\end{equation}

The output with the highest probability is selected as the predicted class label. The following equation summarizes the output as a function of network inputs and weight parameters:

\begin{equation}
\label{eqn:yk}
y_k(\mathbf{\hat x}, \mathbf{w}) =
f\left(
\displaystyle \sum_{j=1}^{M}{w_{kj}^{(2)}h\left(
\displaystyle \sum_{i=1}^{B}{w_{ji}^{(1)}\hat x_i} + w_{j0}^{(1)}
\right)} + w_{k0}^{(2)}
\right)
\end{equation}

The network diagram shown in Figure~\ref{fig:NN} can be extended to the case where there are multiple localizers. We have experimented using two different network structures as shown in Figure~\ref{fig:NN_multiple_localizers}. Even though the accuracy results of both networks were similar, the network shown in Figure~\ref{fig:NN_multiple_localizers}(a) was easier to implement using parallel processing.

\begin{figure}[h]
\centering
\includegraphics[width=0.70\linewidth]{_figure_NN_multiple_localizers_.pdf}
\caption{Two different network structures are used for multiple localizers: (a) the localizer parameters are only connected to a subset of the hidden units (b) fully connected neural network where all localizer parameters are connected to all hidden units. }
\label{fig:NN_multiple_localizers}
\end{figure}