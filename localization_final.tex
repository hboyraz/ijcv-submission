As shown in Figure \ref{fig:WS-ALR}, the first step in recognizing the action is localizing discriminative sub-regions that best describe the action. These candidates are selected using a set of $D$ discriminative sub-region localizers. A localizer ${\phi}_d$, learned during training (as explained in Section \ref{sec:learning}), is a vector of parameters describing the probability distribution of a latent location variable. Even though localizers are not associated with any action class explicitly, using multiple localizers allows the model to select different regions in each frame to capture variations in classes. For every sub-region in each frame of the video, localizers compute the probability of that sub-region being the most discriminative in that frame as shown in Figure~\ref{fig:subregion_prob}.

\begin{figure} [h]
\begin{center}
\includegraphics[width=0.95\linewidth]{_figure_subregion_probability}
\end{center}
\caption{
For a given descriptor frame, we compute the histogram of visual words for each sub-region, $r$, in the frame. Next, we use the sub-region histograms to compute the normalized localization scores for each localizer using the localization filter parameters $\Phi = \{\phi_{1},\dots,\phi_{D}\}$.
 }
\label{fig:subregion_prob}
\end{figure}

Formally, this is implemented with a distribution that is similar to the softmax activation function. If $R_f$ denotes the set of all possible sub-regions in frame $f$, then the probability that the sub-region $r$ is the most discriminative region for localizer $d$, $p^f(r; {\phi}_d)$ is defined as:
\begin{equation}
  \label{eq:localizer_prob}
  p^f(r; {\phi}_d) = \frac{\exp \left({\phi}_d^\top{h}_{f,r}\right)}{\displaystyle\sum_{r'\in R_f}\exp\left({\phi}_d^\top{h}_{f,r'}\right)}
\end{equation}

\noindent where ${h}_{f,r}$ denotes the histogram describing the frequency of visual words in the sub-region $r$ contained in frame $f$. A significant advantage of this linear scoring function is that it can be computed efficiently using the integral image representation, similar to~\cite{vedaldi2009multiple}.