The histogram intersection kernel has proven to be a popular component of a bag of visual words model for object or action recognition. Unfortunately, applying this kernel is difficult because the training data is weakly supervised.  In a traditional kernel-based classifier, new examples are classified by using the kernel function to compute the dot product between transformed versions of the example and various training examples. However, the actual sub-region for each frame in the training videos is not known. While recent work has proposed a methodology for extending kernel methods to latent models, like the latent SVM model \cite{Yang12}, the range of possible values for the latent sub-region location creates significant computational issues.

Instead, we employ the technique introduced by Vedaldi et al.~\cite{Vedaldi12} to approximate non-linear kernels via explicit feature maps. This enables applying the efficient learning methods for linear kernels to non-linear kernels including histogram intersection kernel. An additive kernel is defined by 

\begin{equation}
K(\mathbf{x},\mathbf{y}) = \sum_{b=1}^{B}k(\mathbf{x}_b,\mathbf{y}_b),
\end{equation}

\noindent where $b$ is the bin index. If $k(x,y)$ is homogeneous then the additive kernel is called homogeneous i.e.
\begin{equation}
\forall c \geq 0 : k(cx,cy) = ck(x,y).
\end{equation}

Most popular non-linear kernels used in computer vision applications, including histogram intersection and chi-square kernels, are homogeneous kernels. 

A feature map $\Psi(x)$ for a kernel is defined as a function which maps $x$ into a high-dimensional feature space as follows:

\begin{equation}
\forall x, y : k(x,y) = <\Psi(x),\Psi(y)>.
\end{equation}

Vedaldi et al.~\cite{Vedaldi12} proposed a technique using kernel signatures to analytically construct closed-form feature maps for homogeneous kernels which allows transforming the data into a format suitable for linear classifiers. Also, they propose a method using Fourier transform of the kernel signature and sampling theorem to approximate the infinite dimensional feature map $\Psi(x)$ with the finite dimensional feature map $\hat\Psi(x)$. The feature map approximation of the estimated histogram ${x}^{d}$, where $d$ is the index of one of the $D$ detectors, is given as follows:

\begin {equation}
\frac{[\hat\Psi({x}^{d})]_j} {\sqrt{{x}^{d}L}} = 
\begin{cases}
\sqrt{\kappa(0)}, & j=0,\\
\sqrt{2\kappa(\frac{j+1}{2}L)}\cos{(\frac{j+1}{2}L\log{{x}^{d}})}, & j > 0 \:\textrm{odd}, \\
\sqrt{2\kappa(\frac{j}{2}L)}\sin{(\frac{j}{2}L\log{{x}^{d}})}, & j > 0 \:\textrm{even}, \\
\end{cases}
\end{equation}

\noindent where $j=0,1,...,2n$, $n$ is the number of samples used in the discrete approximation, $L$ is the sampling period, and $\kappa$ is the density function which is given by

\begin {equation}
\kappa(\lambda)=\frac{2}{\pi}\frac{1}{1+4{\lambda}^2}
\end{equation}

\noindent for histogram intersection kernel. In our experiments we set the sampling number $n=3$ which gives a good approximation for the histogram intersection kernel.