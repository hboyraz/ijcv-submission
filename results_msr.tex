In the whole-clip action classification datasets evaluated in the previous section, the training and test data are made up of short clips that have been tightly trimmed temporally to contain only the action.  Thus, the recognition system is only required to locate the action spatially inside the video.  In this section, we show how our system can be adapted to perform recognition when the action must also be located temporally.

This can be evaluated using the MSR-II Action Dataset \cite{Cao10}, which is made up of longer video clips that consist of multiple instances of actions. Since a video sequence may contain different action instances and the duration of each action may be much shorter than the duration of the video squence, we cannot take advantage of aggregating the sub-region histograms over all video frames. We modify our action classification model show in Figure \ref{fig:WS-ALR} as follows:  

\begin{itemize}
\item The histogram aggregation across video frames is removed and each descriptor frame is treated as a set of independent samples. Treating the frames as independent samples does lose temporal coherence, but greatly simplifies the application to streaming data.

\item Each localizer is assigned to a different class label. The localization model in Equation \eqref{eq:localizer_prob} is changed from a distribution over the most discriminative sub-regions in each frame into a joint distribution over sub-region locations and a video label, c :
	\begin{equation}
 	\label{eq:localizer_p_label}
	 p^f(r,c; {\phi}_1, \ldots, {\phi}_C) =\frac{\exp \left({\phi}_c^\top{h}_{f,r}\right)}{\displaystyle\sum_{c'\in C}\displaystyle\sum_{r'\in R_f}\exp\left({\phi}_{c'}^\top{h}_{f,r'}\right)}
	\end{equation}

\noindent where there are $C$ possible labels, and  ${\phi}_1, \ldots, {\phi}_C$ are the localizers associated with each of the C classes.
\item The two-layer neural network is replaced with a multinomial logistic regression classifier. 
\end{itemize}

\begin{figure*}[t]
\begin{center}
\subfigure[]{
  \includegraphics[width=0.32\linewidth]{_figure_MSR_boxing_ROC.pdf}
}
\subfigure[]{
  \includegraphics[width=0.32\linewidth]{_figure_MSR_handwaving_ROC.pdf}
}
\subfigure[]{
  \includegraphics[width=0.32\linewidth]{_figure_MSR_handclapping_ROC.pdf}
}
\end{center}
   \caption{Action detection on MSR-II compared with \cite{Cao10}. Black curves show our result trained with randomly selected 4 videos and Red curves show our detection result trained with 3-fold cross validation.  Training  using just 4 videos, our performance is acceptable. With more training videos, the localization improves significantly. }
\label{fig:MSR_curve}
\end{figure*}

\begin{figure*}[ht]
\begin{center}

\subfigure{
  \includegraphics[width=0.2\linewidth]{_figure_MSR_boxing1.png}
}
\subfigure{
  \includegraphics[width=0.2\linewidth]{_figure_MSR_boxing2.png}
}
\subfigure{
  \includegraphics[width=0.2\linewidth]{_figure_MSR_waving3.png}
}

\subfigure{
  \includegraphics[width=0.2\linewidth]{_figure_MSR_waving2.png}
}
\subfigure{
  \includegraphics[width=0.2\linewidth]{_figure_MSR_clapping1.png}
}
\subfigure{
  \includegraphics[width=0.2\linewidth]{_figure_MSR_clapping2.png}
}
\end{center}
   \caption{Detection Example on MSR dataset. Our method gives reasonable localization results on most of the videos.}
\label{fig:MSR_example}
\end{figure*}


\subsection {Learning Action Detection Model}

During training, each frame is tagged with a label specifying which actions is present. Frames that includes multiple actions are duplicated for each of the actions present in the frame. If a frame doesn't contain an action instance it is labeled as non-action. 

We use multinomial logistic regression for training the action detection model given in Equation \eqref{eq:localizer_p_label}. The learning criterion is created by taking the negative log of the marginal distribution over $c$, then summing this negative log over the frames. For a set of $N_f$ training samples and associated labels $l_1, \dots, l_{N_f}$, the loss function is defined as: 

\begin{equation}
\label{eq:detect_loss_initial}
  L({\Phi}) = -\displaystyle\sum_{f=0}^{N_f}  \log\left(P(T_f=l_f|{\Phi})\right)+ \frac{1}{2}\epsilon\displaystyle \sum_{c=1}^C{||{\phi}_c||^{2}}
\end{equation}

\noindent where ${\Phi} = \{{\phi}_1,\dots,{\phi}_C\}$ are the sub-region localizer weights, $T_f$ is the predicted action label and the $l_f$ is the ground truth action label of sample frame $f$. The probability $P(T_f=l_f|{\Phi})$ is computed by marginalizing the joint distribution in Equation \eqref{eq:localizer_p_label} over $r$:

\begin{equation}
\label{eq:label_prob}
	P(T_f=l_f| {\Phi}) = \frac{\displaystyle\sum_{r\in R_f}\exp \left({\phi}_{l_f}^\top{h}_{f,r}\right)}{\displaystyle\sum_{c\in C}\displaystyle\sum_{r\in R_f}\exp\left({\phi}_{c}^\top{h}_{f,r}\right)}
\end{equation}

Using Equation \eqref{eq:label_prob} in Equation \eqref{eq:detect_loss_initial}, the final loss for $N_f$ examples becomes:
\begin{equation}
  \label{eq:detect_loss} 
	\begin{aligned}
  L({\Phi}) =  & \displaystyle\sum_{f=0}^{N_f}\log \left( \displaystyle\sum_{r\in R_f} {\exp \left({\phi}_{l_f}^\top{h}_{f,r}\right)} \right)\\
              	   & - \displaystyle\sum_{f=0}^{N_f}\log \left({\displaystyle\sum_{c = 1}^C\displaystyle\sum_{r\in R_f}\exp\left({\phi}_{c}^\top{h}_{f,r}\right)}\right) \\
		   & + \frac{1}{2}\epsilon\displaystyle \sum_{c=1}^C{||{\phi}_c||^{2}}
	\end{aligned}
\end{equation}

\noindent A significant advantage of this straightforward probabilistic formulation is that is easily implemented using standard optimization packages. We train the localization parameters, ${\Phi} = \{ {\phi}_c \}$, using conjugate gradient descent optimization package.

\subsection{Inference}
During inference, a video sequence is represented as a set of descriptor frames as explained in Section \ref{sec:video_representation}. For every sub-region in a given descriptor frame, the probability of the sub-region containing action class $c$ is computed using Equation \eqref{eq:localizer_p_label}. If this probability is over a threshold, the sub-region is considered to contain action label $c$. By changing the threshold we can compute the precision and recall curve for each action class as explained in the following section.  

\subsection{Experiments on MSR-II Action Dataset}

In the MSR-II dataset, there are 54 video sequences recorded in crowded scenes. There are 3 type of actions (boxing, handclapping and handwaving) in this dataset and  each video contains multiple instances of actions. There are 203 action instances in total. 

Our experiments on MSR-II dataset shows that the discriminative sub-regions chosen by our weakly-supervised learning approach match the ground-truth labeling of the videos well.
While \cite{Cao10} began by training from the KTH Action dataset, then used videos from the MSR-II dataset to adapt the detectors.  We found that beginning with the KTH dataset did not help our system, so instead we trained directly on four videos from MSR-II as \cite{Cao10} did for domain adaptation, so that both our system and \cite{Cao10} both used the same amount of data from MSR-II. To examine how more data benefits our localization performance, we also randomly split the 54 video sequences into 3 groups and measure our detection performance with a 3-fold cross validation test. 

In detection stage, we perform a per-frame localization and obtain a bounding box for each frame. Following \cite{Cao10}'s evaluation setup, we calculate Precision-Recall curves based on different probability thresholds.  For the precision score, a detection is regarded as a true positive if at least 1/8 of its volume overlaps with that of the ground truth. For recall, the ground truth label is regarded as retrieved if at least 1/8 of its volume is covered by one of the detected volumes. 

As Figure \ref{fig:MSR_curve} shows,  our system produces acceptable results when trained with just four videos, but produces results that improve on \cite{Cao10} when given more access to training data. Our method gives reasonable localization results on most of the videos as shown in Figure \ref{fig:MSR_example}.
